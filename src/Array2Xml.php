<?php

namespace frostbane;

class Array2Xml
{
    private $comments;
    private $cdata;
    private $attribute;
    private $version;
    private $encoding;

    public function __construct(array $options = array())
    {
        $default = array(
            "comments"  => array(
                "<!--",
                "<!---->",
            ),
            "cdata"     => array(
                "<![CDATA[",
                "<![CDATA[]]>",
            ),
            "attribute" => "@",
            "version"   => "1.0",
            "encoding"  => "UTF-8",
        );

        $options = array_merge_recursive($default, $options);

        $this->comments  = $options["comments"];
        $this->cdata     = $options["cdata"];
        $this->attribute = $options["attribute"];
        $this->version   = $options["version"];
        $this->encoding  = $options["encoding"];
    }

    private function getFirstItem(&$array)
    {
        if (count($array) !== 1) {
            return null;
        }

        $element = array_keys($array)[0];

        return $array[$element];
    }

    private function arrayIsNumeric(&$array)
    {
        if (count($array) !== 1) {
            return false;
        }

        $keys = array_keys($this->getFirstItem($array));

        foreach ($keys as $key) {
            if ( !is_numeric($key)) {
                return false;
            }
        }

        return true;
    }

    private function arrayIsComment(&$array)
    {
        if (count($array) !== 1) {
            return false;
        }

        $key = array_keys($array)[0];

        return in_array($key, $this->comments);
    }

    private function arrayIsCData(&$array)
    {
        if (count($array) !== 1) {
            return false;
        }

        $key = array_keys($array)[0];

        return in_array($key, $this->cdata);
    }

    private function escapeCData(&$value)
    {
        $cdataEsc = '<![CDATA[' . str_replace(']]>', ']]]]><![CDATA[>', $value) . ']]>';

        return htmlentities($cdataEsc);
    }

    private function getAttributes(&$array)
    {
        $attributes = array();

        if (is_array($array) && array_key_exists($this->attribute, $array)) {
            $attributes = $array[$this->attribute];

            unset($array[$this->attribute]);

            $value = array_shift($array);
            $array = $value;
        }

        $attributeStr = "";

        foreach ($attributes as $name => $val) {
            $val = htmlentities($val, ENT_QUOTES);

            $attributeStr .= " $name='$val'";
        }

        return ltrim($attributeStr);
    }

    private function dataArray2xml(&$array, $element)
    {
        $xml = "";

        foreach ($array as $index => $value) {
            $attribute = $this->getAttributes($value);

            if (is_array($value) && $this->arrayIsComment($value)) {
                $xml .= "<!-- " . htmlentities(array_values($value)[0]) . " -->";
            } else if (is_array($value) && !empty($value)) {
                $xml .= "<$element $attribute>" . $this->array2xml($value) . "</$element>";
            } elseif (empty($value)) {
                $xml .= "<$element $attribute/>";
            } else {
                $xml .= "<$element $attribute>" . htmlentities($value) . "</$element>";
            }
        }

        return $xml;
    }

    private function array2xml(&$array, $prevElement = "")
    {
        $xml = "";

        foreach ($array as $element => $value) {
            if (is_numeric($element) && empty($value)) {
                continue;
            }

            if (is_numeric($element) && !empty($prevElement)) {
                $element = $prevElement;
            }

            if (is_numeric($element)) {
                $element = "_$element";
            }

            $element = htmlentities($element);

            $attribute = $this->getAttributes($value);

            if (empty($value)) {
                $xml .= "<$element $attribute/>";
            } else if (is_array($value) && $this->arrayIsComment($value)) {
                $xml .= "<!-- " . htmlentities(array_values($value)[0]) . " -->";
            } else if (is_array($value) && $this->arrayIsCData($value)) {
                $xml .= "<$element $attribute><![CDATA[" . $this->escapeCData(array_values($value)[0]) . "]]></$element>";
            } else if (is_array($value) && $this->arrayIsNumeric($array)) {
                $xml = $this->dataArray2xml($value, $element);
            } else if (is_array($value) && !empty($value)) {
                $xml .= "<$element $attribute>" . $this->array2xml($value, $element) . "</$element>";
            } else {
                $xml .= "<$element $attribute>" . htmlentities($value, ENT_QUOTES) . "</$element>";
            }
        }

        return $xml;
    }

    public function createXml(&$data, $root = "root")
    {
        $array  = array($root => $data);
        $xmlStr = $this->array2xml($array);

        simplexml_load_string($xmlStr);

        return "<?xml version='{$this->version}' encoding='{$this->encoding}'?>$xmlStr";
    }
}

